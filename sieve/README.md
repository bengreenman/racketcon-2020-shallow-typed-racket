sieve
===

Example program where:
- deep types is slow,
- flipping one module from deep to shallow makes things fast

computes 66919, the 6667th prime number

deep = main.rkt deep, streams.rkt untyped = 13sec
shallow = main.rkt shallow, streams.rkt untyped = 4sec
untyped = all untyped = 2sec

OK 2sec to 4sec is not bad
