Hi Jay,

Here's a pre-outline, in case you have time/desire to give feedback.
I'm working now on slides for a talk draft.

Right now, I'm thinking points 1 - 4 below will take about equal time.

- - -

1. Typed Racket deal breakers
  - very slow, sometimes
  - cannot run the program, other times (unable to convert type to contract)
  (give examples, go slowly)

2. upcoming Shallow Typed Racket fixes these deal breakers
  - very slow ~> not slow
  - can run more programs (e.g. with syntax objects across boundaries)
  - wow looks great

3. but, Shallow has tradeoffs too
  - when TR is fast, Shallow is slower
  - may miss some runtime "type" errors
  - no blame

4. closer look
  - TR = "best types around" via contracts
  - Shallow = weak types, but via cheap spot-checks
  (examples)

5. both useful, no clear winner, putting choice in your hands
  - just like untyped vs. typed choice, have deep-type vs. shallow-type choice
  - can combine, deep module + shallow module

6. coming soon
  - RFC typed-racket #952
  - pull request #948

