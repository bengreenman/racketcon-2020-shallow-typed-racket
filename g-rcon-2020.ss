#lang at-exp slideshow

;; Slides for 15 min presentation introducing Shallow types for TR
;;
;; Racket Con 2020

;; TODO
;; - [X] deep vs shallow table
;; - [X] reorder codes, left to right
;; - [X] coming soon is wrong color, don't use untyped
;; - [X] same color for results? (code outputs)
;; - [X] weird, show untyped baseline first
;; - [X] repls, get same width
;; - [X] nicer background for email
;; - [X] ditto for tables
;; - [X] rethink devil ... its about tradeoff, Deep not trash ... nevermind
;; - [X] "diff methods" (1) write out? (2) cite Natural + Transient
;; - [X] "more permissive" get same width
;; - [X] stage repl
;; - [X] stage sieves

(require
  file/glob
  pict pict/convert
  (prefix-in pict: pict/shadow)
  pict-abbrevs pict-abbrevs/slideshow gtp-pict
  ppict/2
  racket/draw
  racket/list
  racket/string
  racket/format
  scribble-abbrevs/pict
  slideshow/code
  plot/no-gui (except-in plot/utils min* max*)
  (only-in images/icons/symbol check-icon x-icon)
  (only-in images/icons/misc clock-icon left-magnifying-glass-icon stop-signs-icon)
  images/icons/style)

(define slide-top 4/100)
(define slide-left 4/100)
(define slide-right (- 1 slide-left))
(define slide-bottom 86/100)
(define slide-text-left (* 3/2 slide-left))
(define slide-text-right (- 1 slide-text-left))
(define slide-text-top (* 4 slide-top))
(define slide-text-bottom slide-bottom)

(define slide-text-coord (coord slide-text-left slide-text-top 'lt))
(define slide-text-coord-mid (coord slide-text-left slide-text-top 'lt))
(define slide-text-coord-right (coord slide-text-right slide-text-top 'rt))
(define center-coord (coord 1/2 1/2 'cc))
(define heading-text-coord (coord slide-left slide-top 'lt))
(define heading-text-coord-right (coord slide-right slide-top 'rt))
(define big-landscape-coord (coord 1/2 slide-text-top 'ct))

(define turn revolution)

(define pico-x-sep (w%->pixels 1/100))
(define tiny-x-sep (w%->pixels 2/100))
(define small-x-sep (w%->pixels 5/100))
(define med-x-sep (w%->pixels 10/100))
(define big-x-sep (w%->pixels 15/100))

(define pico-y-sep (h%->pixels 1/100))
(define tiny-y-sep (h%->pixels 2/100))
(define small-y-sep (h%->pixels 5/100))
(define med-y-sep (h%->pixels 10/100))
(define big-y-sep (h%->pixels 15/100))

(define code-line-sep (h%->pixels 2/100))

(define (code-line-append* pp*)
  (apply vl-append code-line-sep pp*))

(define (code-line-append . pp*)
  (code-line-append* pp*))

(define impl-line-sep (h%->pixels 25/1000))

(define (impl-line-append* pp*)
  (apply vl-append impl-line-sep pp*))

(define (impl-line-append . pp*)
  (impl-line-append* pp*))

(define example-code-x-margin (w%->pixels 4/100))
(define example-code-y-margin (h%->pixels 4/100))

;; COLOR
(define black (string->color% "black"))
(define gray (string->color% "light gray"))
(define white (string->color% "white"))
(define transparent (color%-update-alpha white 0))
(define mint-color (hex-triplet->color% #xcef7c3))
(define flag-blue (hex-triplet->color% #x6AB2E7))
(define flag-green (hex-triplet->color% #x12AD2B))
(define flag-red (hex-triplet->color% #xD7141A))
(define balrog-brown (hex-triplet->color% #xC3734E))
(define balrog-blue (hex-triplet->color% #x4090EF))
(define feilong-tan (hex-triplet->color% #xF0C090))
(define feilong-black (hex-triplet->color% #x565656))
(define vega-yellow (hex-triplet->color% #xDFF456))
(define vega-purple (hex-triplet->color% #x986EFB))
(define dhalsim-brown (hex-triplet->color% #xD09060))
(define blanka-green (hex-triplet->color% #xC0F04F))
(define blanka-orange (hex-triplet->color% #xEF6101))
(define cammy-green (hex-triplet->color% #x85A564))
(define chunli-white (hex-triplet->color% #xD0F9FF))
(define ryu-white (hex-triplet->color% #xF0F0F0))
(define ken-red (hex-triplet->color% #xF06000))
(define guile-green (hex-triplet->color% #x50813F))
(define guile-yellow (hex-triplet->color% #xECF175))
(define guile-brown (hex-triplet->color% #xC08042))
(define honda-purple (hex-triplet->color% #x604F81))
(define honda-blue (hex-triplet->color% #x6DA2DC))
(define honda-red (hex-triplet->color% #xDD3A2D))
(define honda-white (hex-triplet->color% #xEBDAE3))
(define honda-litetan (hex-triplet->color% #xF1D293))
(define honda-darktan (hex-triplet->color% #xE19460))
(define honda-grey (hex-triplet->color% #x687598))
(define honda-black (hex-triplet->color% #x030105))
(define typed-color   (hex-triplet->color% #xF19C4D)) ;; orange
(define dark-green (hex-triplet->color% #x697F4D))
(define untyped-color honda-darktan)
(define natural-color honda-purple)
(define transient-color honda-blue)
(define deep-color natural-color)
(define shallow-color transient-color)
(define sand-color (hex-triplet->color% #xFFF7C2))
(define opt-orange (string->color% "Orange"))
(define opt-yellow (string->color% "cadet blue"))
(define opt-green (string->color% "MediumAquamarine"))
(define background-color ryu-white)
(define deco-bg-color honda-grey #;honda-black)

(define Shallow-TR "Shallow TR")
(define Deep-TR "Deep TR")
(define Untyped "Untyped")
(define repl "repl")
(define S-repl "S-repl")

(define racket-logo-mid
  (bitmap "src/racket-med.png"))

(define (tagof str) (string-append "⌊" str "⌋"))
(define tag-T (tagof "T"))

(define title-font
  "FHA Modernized Ideal ClassicNC")

(define body-font "Triplicate T4s")

(define subtitle-font body-font)

(define subsubtitle-font
  "Libian SC")

(define code-font "Inconsolata")
(define lang-font code-font)
(define tu-font title-font)
(define decoration-font title-font)

(define title-size 60)
(define subtitle-size 50)
(define subsubtitle-size 48)
(define body-size 36)
(define caption-size 40)
(define code-size 28)
(define big-body-size 52)
(define tu-size 72)
(define big-node-size 120)

(define shallow-slogan "\"same types, but weaker\"")

(define ((make-string->text #:font font #:size size #:color color) . str*)
  (colorize (text (apply string-append str*) font size) color))

(define (make-string->title #:size [size title-size] #:color [color black])
  (make-string->body #:size size #:color color))

(define (make-string->subtitle #:size [size subtitle-size] #:color [color black])
  (make-string->text #:font subtitle-font #:size size #:color color))

(define (make-string->subsubtitle #:size [size subsubtitle-size] #:color [color black])
  (make-string->text #:font subsubtitle-font #:size size #:color color))

(define (make-string->body #:size [size body-size] #:color [color black])
  (make-string->text #:font body-font #:size size #:color color))

(define (make-string->code #:size [size code-size] #:color [color black])
  (make-string->text #:font code-font #:size size #:color color))
(define ct (make-string->code))
(define bigct (make-string->code #:size (- body-size 4)))
(define cbt (make-string->text #:font (cons 'bold code-font) #:size code-size #:color black))
(define ckwdt cbt)
(define ctypet ct)

(define (tcomment . str*)
  (add-rounded-border
    #:radius 2 #:background-color background-color #:frame-width 1 #:frame-color black #:x-margin tiny-x-sep #:y-margin pico-y-sep
    (apply ct str*)))

(define titlet (make-string->title))
(define small-titlet (make-string->title #:size 42))
(define st (make-string->subtitle))
(define st-blue (make-string->subtitle #:color racket-blue))
(define sbt (make-string->text #:font (cons 'bold subtitle-font) #:size subtitle-size #:color black))
(define sst (make-string->subtitle))
(define sst2 (make-string->subsubtitle #:size (- subsubtitle-size 10)))
(define t (make-string->body))
(define it (make-string->text #:font (cons 'italic body-font) #:size body-size #:color black))
(define bt (make-string->text #:font (cons 'bold body-font) #:size body-size #:color black))
(define tcodesize (make-string->body #:size code-size))
(define btcodesize (make-string->text #:font (cons 'bold body-font) #:size code-size #:color black))

(define (make-flag)
  (define w client-w)
  (define h client-h)
  (define l-box
    (cc-superimpose
      (filled-rectangle (* w 1/3) h #:draw-border? #f #:color white)
      racket-logo-mid))
  (define rt-box
    (filled-rectangle (* w 2/3) (* h 1/2) #:draw-border? #f #:color shallow-color))
  (define rb-box
    (filled-rectangle (* w 2/3) (* h 1/2) #:draw-border? #f #:color deep-color))
  (ht-append l-box (vl-append rt-box rb-box)))

(define (make-usecase-label str)
  (add-rounded-border
    #:radius 4 #:x-margin tiny-x-sep #:y-margin (h%->pixels 14/1000)
    #:background-color background-color #:frame-color honda-black #:frame-width 4
    @tcodesize[str]))

(define (pin-usecase-line pp src-tag find-src tgt-tag find-tgt)
  (pin-line pp (find-tag pp src-tag) find-src (find-tag pp tgt-tag) find-tgt
            #:line-width 4 #:color honda-black))

(struct program-arrow [src-tag src-find tgt-tag tgt-find start-angle end-angle start-pull end-pull color] #:transparent)

(define (add-program-arrow pp arrow #:arrow-size [arrow-size 12] #:line-width [pre-line-width #f] #:style [style 'short-dash] #:label [label (blank)] #:hide? [hide? #false])
  (define line-width (or pre-line-width 3))
  (pin-arrow-line
    arrow-size pp
    (find-tag pp (program-arrow-src-tag arrow))
    (program-arrow-src-find arrow)
    (find-tag pp (program-arrow-tgt-tag arrow))
    (program-arrow-tgt-find arrow)
    #:line-width line-width
    #:label label
    #:hide-arrowhead? hide?
    #:style style
    #:start-angle (program-arrow-start-angle arrow)
    #:end-angle (program-arrow-end-angle arrow)
    #:start-pull (program-arrow-start-pull arrow)
    #:end-pull (program-arrow-end-pull arrow)
    #:color (program-arrow-color arrow)))

(define (add-hubs pp tag)
  (define io-margin 8)
  (define node-padding 6)
  (define h-blank (blank 0 io-margin))
  (define v-blank (blank io-margin 0))
  (vc-append
    node-padding
    (tag-pict v-blank (tag-append tag 'N))
    (hc-append
      node-padding
      (tag-pict h-blank (tag-append tag 'W)) (tag-pict pp tag) (tag-pict h-blank (tag-append tag 'E)))
    (tag-pict v-blank (tag-append tag 'S))))

(define (make-program-pict pp #:radius [pre-radius #f] #:bg-color [bg-color #f] #:frame-color [frame-color #f] #:x-margin [pre-x #f] #:y-margin [pre-y #f])
  (define radius (or pre-radius 12))
  (define x-m (or pre-x small-x-sep))
  (define y-m (or pre-y med-y-sep))
  (add-rounded-border
    pp
    #:radius radius
    #:background-color (or bg-color white)
    #:frame-width 3
    #:frame-color (or frame-color black)
    #:x-margin x-m
    #:y-margin y-m))

(define (make-module-pict pp #:radius radius #:bg-color [bg-color #false] #:x-margin [pre-x #f] #:y-margin [pre-y #f])
  (add-rounded-border
    pp
    #:radius radius
    #:background-color bg-color
    #:frame-width 2
    #:x-margin (or pre-x pico-x-sep)
    #:y-margin (or pre-y pico-y-sep)))

(define (make-typed-pict pp #:x-margin [x #f] #:y-margin [y #f])
  (make-module-pict pp #:radius 37 #:bg-color typed-color #:x-margin x #:y-margin y))

(define (make-untyped-pict pp #:x-margin [x #f] #:y-margin [y #f])
  (make-module-pict pp #:radius 4 #:bg-color untyped-color #:x-margin x #:y-margin y))

(define (make-typed-icon #:font-size [font-size #f] #:width [w #f] #:height [h #f])
  (make-tu-icon "T" #:font-size font-size #:width w #:height h))

(define (make-untyped-icon  #:font-size [font-size #f] #:width [w #f] #:height [h #f])
  (make-tu-icon "U" #:font-size font-size #:width w #:height h))

(define (make-tu-icon str #:font-size [pre-font-size #f] #:width [pre-w #f] #:height [pre-h #f])
  (define font-size (or pre-font-size tu-size))
  (define w (or pre-w 70))
  (define h (or pre-h 70))
  (define str-pict (clip-descent (text str `(bold . ,tu-font) font-size)))
  (cc-superimpose (blank w h) str-pict))

(define U-node (make-untyped-pict (make-untyped-icon)))
(define T-node (make-typed-pict (make-typed-icon)))
(define Lib-node (make-typed-pict (make-tu-icon "Lib") #:x-margin 40 #:y-margin 10))

(define (make-deep-codeblock #:title [title #f] #:x-margin [x #f] #:y-margin [y #f] . pp*)
  (make-deep-codeblock* pp* #:title title #:x-margin x #:y-margin y))

(define (make-shallow-codeblock #:title [title #f] #:x-margin [x #f] #:y-margin [y #f] . pp*)
  (make-shallow-codeblock* pp* #:title title #:x-margin x #:y-margin y))

(define (make-untyped-codeblock #:title [title #f] #:x-margin [x #f] #:y-margin [y #f] . pp*)
  (make-untyped-codeblock* pp* #:title title #:x-margin x #:y-margin y))

(define (make-deep-codeblock* pp* #:title [title #f] #:x-margin [x #f] #:y-margin [y #f])
  (make-codeblock pp* #:title title #:label #f #:bg-color deep-color #:x-margin x #:y-margin y))

(define (make-shallow-codeblock* pp* #:title [title #f] #:x-margin [x #f] #:y-margin [y #f])
  (make-codeblock pp* #:title title #:label #f #:bg-color shallow-color #:x-margin x #:y-margin y))

(define (make-untyped-codeblock* pp* #:title [title #f] #:x-margin [x #f] #:y-margin [y #f])
  (make-codeblock pp* #:title title #:label #f #:bg-color untyped-color #:x-margin x #:y-margin y))

(define codeblock-label-scale 0.65)
(define T-label (scale T-node codeblock-label-scale))
(define U-label (scale U-node codeblock-label-scale))

(define swatch-blank (blank tiny-x-sep med-y-sep))
(define big-swatch-blank (blank big-x-sep big-y-sep))

(define codeblock-radius 4)

(define (make-codeblock pp* #:title [title #f] #:label [label #f] #:bg-color [pre-bg-color #f] #:x-margin [pre-x-margin #f] #:y-margin [pre-y-margin #f])
  (define bg-c (or pre-bg-color (string->color% "lightgray")))
  (define label-margin (if label (* 50/100 (pict-height label)) 0))
  (define (add-label-margin pp [extra 0]) (vl-append (+ extra label-margin) (blank) pp))
  (let* ((block-pict
          (make-program-pict
            #:frame-color bg-c
            #:bg-color (color%-update-alpha bg-c 0.4)
            #:x-margin pre-x-margin
            #:y-margin pre-y-margin
            #:radius codeblock-radius
            (add-label-margin (code-line-append* pp*))))
         (title-pict (and title (tcodesize title))))
    (if label
      (let ((block-pict (add-label-margin block-pict 2)))
        (ppict-do (if title-pict (lt-superimpose block-pict (ht-append 4 (blank) title-pict)) block-pict)
          #:go (coord 1/2 0 'ct) label))
      (if title-pict (vl-append 0 title-pict block-pict) block-pict))))

(define (deep-swatch) (make-deep-codeblock swatch-blank))
(define (shallow-swatch) (make-shallow-codeblock swatch-blank))

(define (big-deep-swatch)
  (make-deep-codeblock
    #:title Deep-TR
    big-swatch-blank))

(define (big-shallow-swatch)
  (make-shallow-codeblock
    #:title Shallow-TR
    big-swatch-blank))

(define (make-optimization-arrow [angle #f])
  (arrowhead (w%->pixels 5/100) (or angle (* 3/4 turn))))

(define topleft-coord (coord 2/100 6/100 'lt))
(define (source-coord #:abs-x [abs-x 0] #:abs-y [abs-y 0])
  (coord 20/100 48/100 'cc #:abs-x abs-x #:abs-y abs-y))
(define (natural-coord #:abs-x [abs-x 0] #:abs-y [abs-y 0])
  (coord 50/100 25/100 'lc #:abs-x abs-x #:abs-y abs-y))
(define (transient-coord #:abs-x [abs-x 0] #:abs-y [abs-y 0])
  (coord 50/100 75/100 'lc #:abs-x abs-x #:abs-y abs-y))

(define (make-nametag pp)
  (add-rounded-border
    #:x-margin tiny-x-sep #:y-margin tiny-y-sep #:radius 6 #:background-color white
    #:frame-width 6 #:frame-color flag-red
    pp))

(define (add-natural-transient-arrows pp)
  (let* ((arr* (list (program-arrow 'source rt-find 'N-W lc-find (* 2/100 turn) 0 (* 1/10 turn) (* 1/6 turn) black)
                     (program-arrow 'source rb-find 'T-W lc-find (* 96/100 turn)  0 (* 1/10 turn) (* 1/6 turn) black))))
    (for/fold ((acc pp))
              ((arr (in-list arr*)))
      (add-program-arrow
        acc arr
        #:arrow-size 15
        #:line-width 5
        #:style 'solid))))

(define (check-item->text x)
  (cond
    [(eq? #f x)
     (t "check ....")]
    [(string? x)
     (t (string-append "check " x))]
    [(list? x)
     (vl-append
       tiny-y-sep
       (blank 0 tiny-y-sep)
       (t (string-append "check " (car x)))
       (t (string-append "& " (cadr x))))]
    [else
      (raise-argument-error 'check-item->text "(or/c #f string? list?)" x)]))

(define (make-pipeline . pp*)
  (make-pipeline* pp*))

(define (make-pipeline* pp*)
  (for/fold ((acc (blank)))
            ((pp (in-list pp*))
             (i (in-naturals)))
    (vl-append
      pico-y-sep
      acc
      (hb-append (blank (* i med-x-sep) 0) pp))))

(define (split-list x*)
  (cond
    [(null? x*)
     (values '() '())]
    [(null? (cdr x*))
     (values x* '())]
    [else
      (define-values [top* bot*] (split-list (cddr x*)))
      (values (cons (car x*) top*) (cons (cadr x*) bot*))]))

(define (make-x-step #:border-color bc str)
  (add-rounded-border
    #:radius 8 #:frame-color bc #:frame-width 8 #:background-color white
    #:x-margin tiny-x-sep #:y-margin tiny-y-sep
    (t str)))

(define (make-natural-step str)
  (make-x-step #:border-color natural-color str))

(define (make-transient-step str)
  (make-x-step #:border-color transient-color str))

(define (make-danger-step str)
  (make-x-step #:border-color flag-red str))

(define (bullet-append . pp*)
  (bullet-append* pp*))

(define (bullet-append* pp*)
  (ht-append @t{- } (apply vl-append tiny-y-sep pp*)))

(define *opt-color-level* (make-parameter 0))

(define (danger-opt str [level #f])
  (make-opt str (and (< 0 (or level (*opt-color-level*))) flag-red)))

(define (risk-opt str [level #f])
  (make-opt str (and (< 1 (or level (*opt-color-level*))) opt-orange)))

(define (tag-opt str [level #f])
  (make-opt str (and (< 2 (or level (*opt-color-level*))) opt-yellow)))

(define (fun-opt str [level #f])
  (make-opt str (and (< 3 (or level (*opt-color-level*))) opt-green)))

(define (safe-opt str)
  (make-opt str))

(define (make-opt str [pre-color #f])
  (define color (or pre-color black))
  (add-rounded-border
    #:radius 10 #:x-margin small-x-sep #:y-margin small-y-sep
    #:frame-width (if pre-color 14 4) #:frame-color color #:background-color white
    (t str)))

(define (make-optimization-table [color-level 0])
  (parameterize ((*opt-color-level* color-level))
    (table
      4
      (list
        (risk-opt "apply")
        (safe-opt "box")
        (danger-opt "dead-code")
        (safe-opt "extflonum")
        (safe-opt "fixnum")
        (safe-opt "float-complex")
        (fun-opt "float")
        (tag-opt "list")
        (fun-opt "number")
        (danger-opt "pair")
        (tag-opt "sequence")
        (safe-opt "string")
        (safe-opt "struct")
        (fun-opt "unboxed-let")
        (safe-opt "vector")
        (blank))
      cc-superimpose cc-superimpose tiny-x-sep small-y-sep)))

(define opt-y 20/100)

(define (mk-codeblock sym)
  (case sym
    ((deep) make-deep-codeblock*)
    ((shallow) make-shallow-codeblock*)
    ((untyped) make-untyped-codeblock*)
    (else (raise-argument-error 'mk-codeblock "(or/c 'deep 'shallow 'untyped)" sym))))

(define (mk-lang sym)
  (define-values [d-pict s-pict u-pict]
    (let* ((s-pict @ct{#lang shallow}))
      (values
        (lt-superimpose (ghost s-pict) @ct{#lang typed/rkt})
        s-pict
        (lt-superimpose (ghost s-pict) @ct{#lang racket}))))
  (case sym
    ((deep)    d-pict)
    ((shallow) s-pict)
    ((untyped) u-pict)
    (else (raise-argument-error 'mk-lang "(or/c 'deep 'shallow 'untyped)" sym))))

(define (mk-letter sym)
  (case sym
    ((deep) "D")
    ((shallow) "S")
    ((untyped) "U")
    (else (raise-argument-error 'mk-letter "(or/c 'deep 'shallow 'untyped)" sym))))

(define (mk-title sym str)
  (format "~a-~a" (mk-letter sym) str))

(define (codeblock-append lhs-pict rhs-pict)
  (ht-append pico-x-sep lhs-pict rhs-pict))

(define (codeblock-v-append top-pict bot-pict)
  (vl-append pico-y-sep top-pict bot-pict))

(define (make-sieve rhs lhs)
  (define final-line @ct{(nth-prime 6667)})
  (define rhs-pict
    ((mk-codeblock rhs)
     #:title (mk-title rhs "main.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (list
       (mk-lang rhs)
       @ct{....}
       final-line)))
  (define lhs-pict
    ((mk-codeblock lhs)
        #:title (mk-title lhs "streams.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
        (list
          (mk-lang lhs)
          @ct{....}
          (ghost final-line))))
  (codeblock-append lhs-pict rhs-pict))

(define (make-ho-any rhs lhs)
  (define rhs-pict
    ((mk-codeblock rhs)
     #:title (mk-title rhs "main.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (list
       (mk-lang rhs)
       @ct{(require "@mk-title[rhs "box.rkt"]")}
       @ct{}
       @ct{(set-box! b 0)})))
  (define lhs-pict
    ((mk-codeblock lhs)
     #:title (mk-title lhs "box.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (list
       (mk-lang lhs)
       @ct{(provide b)}
       @ct{}
       @ct{(: b Any)}
       @ct{(define b (box 42))})))
  (codeblock-append lhs-pict rhs-pict))

(define (make-index-of sym)
  (define code-pict
    ((mk-codeblock sym)
     #:title (mk-title sym "main.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (if (eq? sym 'untyped)
       (list
         (mk-lang sym)
         @ct{}
         @ct{(index-of (list 'a 'b) 'a)})
       (list
         (mk-lang sym)
         @ct{(require/typed racket/list}
         @ct{  [index-of}
         @ct{   (All (T)}
         @ct{    (-> (Listof T) T}
         @ct{        (U #f Natural)))])}
         @ct{ }
         @ct{(index-of (list 'a 'b) 'a)}))))
  code-pict)

(define (maybe-blank yes? p)
  (if yes? (blank) p))

(define (maybe-ghost yes? p)
  (if yes?
    (ghost p)
    p))

(define (maybe-ghost* yes? p*)
  (for/list ((p (in-list p*)))
    (maybe-ghost yes? p)))

(define (make-boundaries lhs api rhs #:keep-blank? [keep-blank? #f])
  (define lhs-pict
    ((mk-codeblock lhs)
     #:title (mk-title lhs "main.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (maybe-ghost*
      keep-blank?
      (list
       (mk-lang lhs)
       @ct{(require "@mk-title[api "api.rkt"]")}
       @ct{}
       @ct{(define (f n) (+ n 1))}
       @ct{(build-list 4 f)}))))
  (define api-pict
    ((mk-codeblock api)
     #:title (mk-title api "api.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (maybe-ghost*
      keep-blank?
      (list
       (mk-lang api)
       @ct{(require/typed/provide "@mk-title[rhs "lib.rkt"]"}
       @ct{  [build-list}
       @ct{   (-> Integer (-> Natural Any)}
       @ct{       (Listof Any))])}))))
  (define rhs-pict
    ((mk-codeblock rhs)
     #:title (mk-title rhs "lib.rkt") #:x-margin example-code-x-margin #:y-margin example-code-y-margin
     (maybe-ghost*
       keep-blank?
       (list
         (mk-lang rhs)
         @ct{(define (build-list n f)}
         @ct{  .... (f "oops") ....)}))))
  (codeblock-append lhs-pict (codeblock-v-append api-pict rhs-pict)))

(define (scale-browser-screenshot str)
  (scale-to-fit (bitmap str) (w%->pixels 9/10) client-h))

(define summary-good-pict
  (bitmap (check-icon #:material metal-icon-material #:height 50)))

(define summary-bad-pict
  (bitmap (x-icon #:material metal-icon-material #:height 45)))

(define summary-w (w%->pixels 30/100))
(define summary-h (h%->pixels 30/100))

(define (summary-scale pict [good? 'none])
  (define small-pict (scale-to-fit pict summary-w summary-h))
  (ppict-do
    small-pict
    #:go (coord 0 0 'cc)
    (if (eq? good? 'none) (blank) (if good? summary-good-pict summary-bad-pict))))

(define (make-big-codeblock sym [lang-only? #f])
  (define big-bg
    (blank (- client-w (w%->pixels 2/100))
           (- (* 1/2 client-h) (h%->pixels 2/100))))
  (define big-lang
    (ppict-do
      big-bg
      #:go (coord 0 0 'lt #:abs-x example-code-x-margin #:abs-y example-code-y-margin #:sep (* 1.7 code-line-sep))
      (tag-pict (case sym
        ((deep) @ct{#lang typed/racket  ; or, typed/racket/deep})
        ((shallow) @ct{#lang typed/racket/shallow})
        (else (raise-user-error 'make-big-codeblock "(or/c 'deep 'shallow)" sym))) 'lang)
      (blank 0 10)
      (if lang-only? (blank) (tag-pict @ct{(: x (Listof Integer))} 'int))
      @ct{}
      (if lang-only? (blank) (tag-pict @ct{(: f (-> String String))} 'fun))))
  ((mk-codeblock sym)
   #:x-margin 0 #:y-margin 0
   (list big-lang)))

(define (at-comment sym)
  (at-find-pict sym lb-find 'lt #:abs-y pico-y-sep #:abs-x med-x-sep #:sep pico-y-sep))

(define (at-comment-right sym)
  (at-find-pict sym rc-find 'lt #:abs-x tiny-x-sep))

(define (make-repl-table row*)
  (make-2table row*
    #:col-align lc-superimpose
    #:row-align ct-superimpose
    #:col-sep small-x-sep
    #:row-sep small-y-sep))

(define untyped-add2
  (add-hubs
   (make-untyped-codeblock*
    #:title Untyped #:x-margin example-code-x-margin #:y-margin example-code-y-margin
    (list
      (mk-lang 'untyped)
      @ct{}
      @ct{(define (add2 n)}
      @ct{  (+ n 2))})) 'U))

(define deep-add2
  (add-hubs
   (make-deep-codeblock*
    #:title Deep-TR #:x-margin example-code-x-margin #:y-margin example-code-y-margin
    (list
      (mk-lang 'deep)
      @ct{(: add2 (-> Natural Natural))}
      @ct{(define (add2 n)}
      @ct{  (+ n 2))})) 'T))

(define shallow-add2
  (add-hubs
   (make-shallow-codeblock*
    #:title Shallow-TR #:x-margin example-code-x-margin #:y-margin example-code-y-margin
    (list
      (mk-lang 'shallow)
      @ct{(: add2 (-> Natural Natural))}
      @ct{(define (add2 n)}
      @ct{  (+ n 2))})) 'S))

(define (make-small-codeblock sym)
  (scale
    ((mk-codeblock sym)
     (list
       (mk-lang sym)
       (blank (w%->pixels 24/100) (h%->pixels 7/100))))
    0.8))

(define (make-enforce sym)
  (define code-pict (make-small-codeblock sym))
  (case sym
    ((deep) (make-deep-enforce code-pict))
    ((shallow) (make-shallow-enforce code-pict))
    (else (raise-argument-error 'make-enforce "(or/c 'deep 'shallow)" sym))))

(define (make-deep-enforce pp)
  (define guard-pict (bitmap (stop-signs-icon #:height 60)))
  (ppict-do
    pp
    #:go (coord 0 1/2 'cc)
    guard-pict
    #:go (coord 1 1/2 'cc)
    guard-pict))

(define (make-shallow-enforce pp)
  (define spot (bitmap (left-magnifying-glass-icon #:height 36)))
  (for/fold ((pp pp))
            ((crd (list (coord 20/100 35/100)
                        (coord 30/100 80/100)
                        (coord 50/100 50/100)
                        (coord 63/100 85/100)
                        (coord 80/100 47/100))))
    (ppict-do pp #:go crd spot)))

(define (make-rhombus w h c)
  (define (draw dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush
          (new brush%
               [style 'solid]
               [color c]))
    (send dc set-pen
          (new pen% [width 1] [color honda-black]))
    (define path (new dc-path%))
    (send path move-to 0 (* 1/2 h))
    (send path line-to (* 1/2 w) 0)
    (send path line-to w (* 1/2 h))
    (send path line-to (* 1/2 w) h)
    (send path close)
    (send dc draw-path path dx dy)
    ;; --
    (send dc set-brush old-brush)
    (send dc set-pen old-pen))
  (dc draw w h))

(define (migration-rectangle w h #:flag [flag* '()])
  (define (draw dc dx dy)
    (define old-brush (send dc get-brush))
    (define old-pen (send dc get-pen))
    (send dc set-brush
          (new brush%
               [gradient
                (new linear-gradient%
                     [x0 dx] [y0 dy]
                     [x1 (+ dx w)] [y1 dy]
                     [stops (list (list 0 untyped-color)
                                  (list 40/100 shallow-color)
                                  (list 60/100 shallow-color)
                                  (list 100/100 deep-color))])]))
    (send dc set-pen (new pen% [width 1] [color honda-litetan]))
    (define path (new dc-path%))
    (send path rectangle 0 (* 1/2 h) w (* 1/2 h))
    (send dc draw-path path dx dy)
    ;; --
    (send dc set-brush old-brush)
    (send dc set-pen old-pen))
  (let* ((pp (dc draw w h))
         (gpp (blank (pict-width pp) (pict-height pp)))
         (shallow? (memq 'shallow flag*))
         (deep? (memq 'deep flag*))
         (s0 's0) (s1 's1) (d0 'd0) (d1 'd1)
         (s00 's00) (s10 's10) (d00 'd00) (d10 'd10))
    (ppict-do pp
      #:go (at-find-pict pp lb-find 'lt #:abs-x tiny-x-sep #:abs-y pico-y-sep) @tcodesize{Untyped}
      #:go (at-find-pict pp cb-find 'ct #:abs-y pico-y-sep) @tcodesize{Shallow}
      #:go (at-find-pict pp rb-find 'rt #:abs-x (- tiny-x-sep) #:abs-y pico-y-sep) @tcodesize{Deep}
      ;; -- s0 flag
      #:go (at-find-pict pp cc-find 'cc #:abs-x (- small-x-sep))
      (tag-pict (blank) s0)
      #:go (at-find-pict pp cc-find 'rb #:abs-x (- (* 1.2 small-x-sep)) #:abs-y (- (* 0.8 small-y-sep)))
      (if (not shallow?) (blank) (tag-pict (make-usecase-label "Fix perf.") s00))
      #:set (let ((pp ppict-do-state))
              (if (not shallow?) pp (pin-usecase-line pp s0 cc-find s00 rb-find)))
      ;; --- s1 flag
      #:go (at-find-pict pp cc-find 'cc #:abs-x small-x-sep)
      (tag-pict (blank) s1)
      #:go (at-find-pict pp cc-find 'rb #:abs-x (* 1.2 small-x-sep) #:abs-y (- med-y-sep))
      (if (not shallow?) (blank) (tag-pict (make-usecase-label "Avoid error") s10))
      #:set (let ((pp ppict-do-state))
              (if (not shallow?) pp (pin-usecase-line pp s1 cc-find s10 rb-find)))
      ;; --- d0 flag
      #:go (at-find-pict pp rc-find 'cc #:abs-x (- med-x-sep))
      (tag-pict (blank) d0)
      #:go (at-find-pict pp rc-find 'rb #:abs-x (- (* 1.2 med-x-sep)) #:abs-y (- (* 1.2 med-y-sep)))
      (if (not deep?) (blank) (tag-pict (make-usecase-label "Guarantees") d00))
      #:set (let ((pp ppict-do-state))
              (if (not deep?) pp (pin-usecase-line pp d0 cc-find d00 rb-find)))
      ;; --- d1 flag
      #:go (at-find-pict pp rc-find 'cc #:abs-x (- small-x-sep))
      (tag-pict (blank) d1)
      #:go (at-find-pict pp rc-find 'rb #:abs-x (- (* 1.1 small-x-sep)) #:abs-y (- (* 0.7 small-y-sep)))
      (if (not deep?) (blank) (tag-pict (make-usecase-label "Fully-typed") d10))
      #:set (let ((pp ppict-do-state))
              (if (not deep?) pp (pin-usecase-line pp d1 cc-find d10 rb-find)))
      )))

(define (make-migration-path)
  (let* ((my-blank (blank tiny-x-sep tiny-y-sep))
         (lhs (add-hubs (make-untyped-codeblock my-blank) 'U))
         (mid (add-hubs (make-shallow-codeblock my-blank) 'S))
         (rhs (add-hubs (make-deep-codeblock my-blank) 'T))
         (pp (ht-append med-x-sep lhs mid rhs)))
    (for/fold ((pp pp))
              ((arr (in-list
                      (list
                        (program-arrow 'U-E rt-find 'S-W lt-find 0 0 0 0 black)
                        (program-arrow 'S-E rt-find 'T-W lt-find 0 0 0 0 black)))))
      (add-program-arrow pp arr #:style 'solid))))

(define (make-coming-soon)
  (let* ((pp
          (values  ;add-rounded-border
             ; #:radius 4 #:background-color honda-litetan
             ; #:frame-width 3 #:frame-color honda-darktan
             ; #:x-margin tiny-x-sep #:y-margin tiny-y-sep
            (make-2table
              #:col-align lc-superimpose
              #:row-align ct-superimpose
              #:col-sep tiny-x-sep
              #:row-sep tiny-y-sep
              (list
                (cons (blank) (blank))
                (cons @tcodesize{RFC} (make-pull "952"))
                (cons @tcodesize{PR}  (make-pull "948"))))))
         (w (pict-width pp))
         (cp (bgbg (cc-superimpose (blank (pict-width pp) 0) @tcodesize{~~  coming soon  ~~}))))
    (vc-append 4 cp pp)))

(define (add-star-background pp)
  (cc-superimpose
    (make-rhombus (* 1.4 (pict-width pp)) (* 3.8 (pict-height pp)) vega-yellow)
    (vl-append 4 (blank) pp)))

(define (make-bullets b-str str*)
  (make-2table
    #:col-sep tiny-x-sep
    #:row-sep tiny-y-sep
    #:col-align lt-superimpose
    #:row-align lt-superimpose
    (map (lambda (s) (cons (tcodesize b-str) (tcodesize s))) str*)))

(define (make-pull num)
  (tcodesize (string-append "typed-racket/pull/" (~a num))))

(define (make-title-stripe)
  (vl-append (filled-rectangle client-w (* client-h 1/10) #:draw-border? #f #:color deep-color)
             (filled-rectangle client-w (* client-h 1/10) #:draw-border? #f #:color shallow-color)))

(define slogan-coord (coord 0 0 'lt))

(define catchphrase-sym 'catchphrase)
(define chap-sym 'chaperone)
(define pro-sym 'pros)
(define con-sym 'cons)
(define cometh-sym 'cometh)
(define bg-sym 'bg)

(define (make-slogan deco*)
  (define catchphrase? (memq catchphrase-sym deco*))
  (define chap? (memq chap-sym deco*))
  (define pro? (memq pro-sym deco*))
  (define con? (memq con-sym deco*))
  (define cometh? (memq cometh-sym deco*))
  (define bg? (memq bg-sym deco*))
  (define stripe-y 28/100)
  (define below-stripe-y 52/100)
  (define title-x 8/100)
  ;;
  (ppict-do
    (blank client-w client-h)
    #:go (coord 1/2 stripe-y 'ct) (make-title-stripe)
    #:go (coord title-x 10/100 'lt)
    (let ((tt @titlet{Shallow Typed Racket}))
      (if catchphrase?
        (vl-append tiny-y-sep tt @t{  "same types, but weaker"})
        tt))
    #:go (coord 71/100 (- stripe-y 1/100) 'ct)
    (maybe-blank
      (not chap?)
      (let ((tt ((make-string->subtitle #:size (+ 6 body-size)) "No Chaperones!")))
        (add-star-background
          tt)))
    #:go (coord title-x below-stripe-y 'lt)
    (let* ((pro-bull (make-bullets "+" (list "fast boundaries" "more expressive" "simple")))
           (con-bull (make-bullets "-" (list "slow @ fully-typed" "temporary")))
           (bull-blank (blank (max (pict-width pro-bull) (pict-width con-bull)) 0))
           (pro-pp
             (make-codeblock
               #:bg-color cammy-green
               #:x-margin tiny-x-sep #:y-margin tiny-y-sep
               (list (lc-superimpose bull-blank pro-bull))))
           (con-pp
             (make-codeblock
               #:bg-color honda-red
               #:x-margin tiny-x-sep #:y-margin tiny-y-sep
               (list (lc-superimpose bull-blank con-bull)))))
      (if (not pro?)
        (blank)
        (if (not con?)
          pro-pp
          (vl-append tiny-y-sep pro-pp con-pp))))
    #:go (coord 48/100 below-stripe-y 'lt)
    (maybe-blank
      (not cometh?)
      (make-coming-soon))
    #:go (coord 95/100 80/100 'rb)
    (maybe-blank (not bg?) (vr-append tiny-y-sep @t{Ben Greenman} @t{2020-10-18}))))

(define repl-bot-wedge
  (blank (pict-width @ct{(define (make-random-list))}) 0))

(define (bgbg pp)
  (make-codeblock
      #:bg-color deco-bg-color
      #:x-margin tiny-x-sep #:y-margin tiny-y-sep
      (list pp)))

(define (make-datatable row*)
  (define title* (map btcodesize (car row*)))
  (vc-append
    (bgbg
      (table 3 title*
        (cons lc-superimpose rc-superimpose) rc-superimpose
        small-x-sep 0))
    (table
      3
      (apply append
             (cons
               (map (lambda (x) (blank (pict-width x) 0)) title*)
               (map (lambda (x) (map tcodesize x)) (cdr row*))))
      (cons lc-superimpose rc-superimpose)
      rc-superimpose
      small-x-sep
      (h%->pixels 4/100))))

;; =============================================================================

(define (do-show)
  (set-page-numbers-visible! #false)
  (set-spotlight-style! #:size 60 #:color (color%-update-alpha highlight-brush-color 0.6))
  ;; --
  (parameterize ((current-slide-assembler (slide-assembler/background (current-slide-assembler) #:color background-color)))
    (sec:title)
    (sec:big-picture)
    (sec:shallow-fast)
    (sec:shallow-expressive)
    (sec:shallow-simple)
    (sec:shallow-bad)
    (sec:cometh-soon)
    (pslide)
    (void))
  (void))

;; -----------------------------------------------------------------------------

(define (sec:title)
  (pslide
    #:go slogan-coord (make-slogan (list bg-sym)))
  (void))

(define (sec:big-picture)
  (pslide
    #:alt [(make-slogan (list ))]
    (make-slogan (list catchphrase-sym)))
  (pslide
    ;; same type checker, Deep T error iff Shallow T error
    ;;  not like no-check, get same type checker really
    ;;  in fact more expressive, will come to that
    ;; ...
    #:go heading-text-coord
    @t{same static types ...}
    #:go (coord 20/100 1/2 'cc)
    untyped-add2
    #:go (coord 70/100 slide-text-top 'ct)
    deep-add2
    #:set
    (let ((pp ppict-do-state))
      (add-program-arrow pp
                         (program-arrow 'U-E rt-find 'T-W lc-find 0 0 1/2 1/2 black)
                         #:style 'solid))
    #:next
    #:go (coord 70/100 88/100 'cb)
    shallow-add2
    #:set (let ((pp ppict-do-state))
            (add-program-arrow pp
                               (program-arrow 'U-E rb-find 'S-W lc-find 0 0 1/2 1/2 black)
                               #:style 'short-dash))
    )
  (let* ((lhs
          (make-deep-codeblock*
            #:title Deep-TR
            (list
              @ct{(define (make-random-list)}
              @ct{  : (Listof String)}
              @ct{  ....)}
              repl-bot-wedge)))
         (r0
          (list
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}))
         (r1
          (list
            @ct{> (make-random-list)}
            @ct{'()}
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}))
         (r2
          (list
            @ct{> (make-random-list)}
            @ct{'()}
            @ct{> (make-random-list)}
            @ct{'("A" "B" "C")}
            @ct{> (make-random-list)}
            @ct{}))
         (r3
          (list
            @ct{> (make-random-list)}
            @ct{'()}
            @ct{> (make-random-list)}
            @ct{'("A" "B" "C")}
            @ct{> (make-random-list)}
            @ct{contract error})))
    ;; TODO could make slides in a loop
    (pslide
      #:go heading-text-coord-right
      @t{... but weaker}
      #:go (coord slide-text-left slide-text-top 'lt)
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r0))))]
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r1))))]
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r2))))]
      (make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r3))))))
  (let* ((lhs
          (make-shallow-codeblock*
            #:title Shallow-TR
            (list
              @ct{(define (make-random-list)}
              @ct{  : (Listof String)}
              @ct{  ....)}
              repl-bot-wedge)))
         (r0
          (list
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}))
         (r2
          (list
            @ct{> (make-random-list)}
            @ct{'("A")}
            @ct{> (make-random-list)}
            @ct{contract error}
            @ct{> (make-random-list)}
            @ct{}
            @ct{> (make-random-list)}
            @ct{}))
         (r3
          (list
            @ct{> (make-random-list)}
            @ct{'("A")}
            @ct{> (make-random-list)}
            @ct{contract error}
            @ct{> (make-random-list)}
            @ct{'(a 2 "c")}
            @ct{> (make-random-list)}
            @ct{'(cheese (pizza))})))
    (pslide
      #:go heading-text-coord-right
      @t{... but weaker}
      #:go (coord slide-text-left slide-text-top 'lt)
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r0))))]
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r2))))]
      (make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r3))))))
  (let* ((lhs
          (make-deep-codeblock*
            #:title Deep-TR
            (list
              @ct{(define (make-random-fn)}
              @ct{  : (-> String)}
              @ct{  ....)}
              repl-bot-wedge)))
         (r0
          (list
            @ct{> (make-random-fn)}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}))
         (r1
          (list
            @ct{> (make-random-fn)}
            @ct{#<procedure>}
            @ct{> ((make-random-fn))}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}))
         (r2
          (list
            @ct{> (make-random-fn)}
            @ct{#<procedure>}
            @ct{> ((make-random-fn))}
            @ct{"hello"}
            @ct{> ((make-random-fn))}
            @ct{contract error})))
    (pslide
      #:go heading-text-coord-right
      @t{... but weaker}
      #:go (coord slide-text-left slide-text-top 'lt)
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r0))))]
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r1))))]
      (make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r2))))))
  (let* ((lhs
          (make-shallow-codeblock*
            #:title Shallow-TR
            (list
              @ct{(define (make-random-fn)}
              @ct{  : (-> String)}
              @ct{  ....)}
              repl-bot-wedge)))
         (r0
          (list
            @ct{> ((make-random-fn))}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}))
         (r1
          (list
            @ct{> ((make-random-fn))}
            @ct{"hello"}
            @ct{> ((make-random-fn))}
            @ct{}
            @ct{> ((make-random-fn))}
            @ct{}))
         (r2
          (list
            @ct{> ((make-random-fn))}
            @ct{"hello"}
            @ct{> ((make-random-fn))}
            @ct{42}
            @ct{> ((make-random-fn))}
            @ct{'()}))
         (row0
          (cons lhs (make-untyped-codeblock* #:title repl r2))))
    (pslide
      #:go heading-text-coord-right
      @t{... but weaker}
      #:go (coord slide-text-left slide-text-top 'lt)
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r0))))]
      #:alt [(make-repl-table (list (cons lhs (make-untyped-codeblock* #:title repl r1))))]
      #:alt [(make-repl-table (list row0))]
      (make-repl-table
        (list
          row0
          (cons
            (blank)
            (make-shallow-codeblock*
              #:title S-repl
              (list
                @ct{> ((make-random-fn))}
                @ct{contract error})))))))
  (pslide
    #:go heading-text-coord-right
    @t{... but weaker}
    #:go (coord slide-text-left slide-text-top 'lt)
    (make-repl-table
      (list
        (cons
          (big-deep-swatch)
          (code-line-append
            @tcodesize{}
            @tcodesize{guarantees full types}
            @tcodesize{ everywhere}))
        (cons
          (big-shallow-swatch)
          (code-line-append
            @tcodesize{}
            @tcodesize{guarantees type-shapes}
            @tcodesize{ only in typed code})))))
  (pslide
    #:go heading-text-coord
    @t{Different guarantees => diff. methods}
    #:go (coord slide-text-left slide-text-top 'lt)
    (make-repl-table
      (list
        (cons
          (make-deep-enforce
            (big-deep-swatch))
          (impl-line-append
            (blank 0 8)
            @tcodesize{=> enforce full types}
            @tcodesize{    with contracts}
            @tcodesize{    at boundaries to non-deep code}
            ))
        (cons
          (make-shallow-enforce
            (big-shallow-swatch))
          (impl-line-append
            (blank 0 10)
            @tcodesize{=> check type-shapes}
            @tcodesize{    with asserts}
            @tcodesize{    on every line of shallow code}
            ))
        ))
    #:go (coord slide-text-right 88/100 'rt)
    @tcodesize{[ Thanks Sam Tobin-Hochstadt & Michael M. Vitousek ]})
  (pslide
    #:go slogan-coord
    #:alt [(make-slogan (list catchphrase-sym))]
    #:alt [(make-slogan (list catchphrase-sym chap-sym))]
    (make-slogan (list catchphrase-sym chap-sym pro-sym)))
  (void))

(define (sec:shallow-fast)
  (let* ((u-row
          (cons (make-sieve 'untyped 'untyped) @t{~2 sec.}))
         (d-row
          (cons (make-sieve 'deep 'untyped) @t{~13 sec.}))
         (s-row
          (cons (make-sieve 'shallow 'untyped) @t{~4 sec.})))
    (pslide
      #:go heading-text-coord
      @t{Deep types can be slow}
      #:go (coord slide-text-left 20/100 'lt)
      (make-2table #:row-sep med-y-sep #:col-align lc-superimpose (list u-row d-row)))
    (pslide
      #:go heading-text-coord
      @t{Shallow types can be faster}
      #:go (coord slide-text-left 20/100 'lt)
      (make-2table #:row-sep med-y-sep #:col-align lc-superimpose (list d-row s-row))))
  (pslide
    #:go heading-text-coord
    @t{Deep vs. Shallow, worst-case overhead}
    #:go (coord 1/2 20/100 'ct)
    (make-datatable
      (list
        (list "Benchmark" "Worst Deep" "Worst Shallow")
        (list "sieve" "10x" "2x")
        (list "fsmoo" "451x" "4x")
        (list "dungeon" "14000x" "5x")
        (list "mbta" "2x" "2x")
        (list "tetris" "12x" "8x")
        (list "synth" "49x" "4x"))))
  (void))

(define (sec:shallow-expressive)
  (pslide
    #:go heading-text-coord
    @t{Deep types can be strict}
    #:go (coord slide-text-left 20/100 'lt)
    #:next
    #:alt [(bgbg (scale-browser-screenshot "src/racket-users-ho-any.png"))]
    (vc-append
      med-y-sep
      (make-2table
        #:row-sep med-y-sep
        #:col-align lc-superimpose
        (list
          (cons (make-ho-any 'untyped 'deep)
                @t{Error})))
      @tcodesize{attempted to use higher-order value passed as Any}))
  (pslide
    #:go heading-text-coord
    @t{Shallow types are more permissive}
    #:go (coord slide-text-left 20/100 'lt)
    (vc-append
      med-y-sep
      (make-2table
        #:row-sep med-y-sep
        #:col-align lc-superimpose
        (list
          (cons (make-ho-any 'untyped 'shallow)
                @t{(void)})))
      @tcodesize{OK to use higher-order value passed as Any}))
  (void))

(define (sec:shallow-simple)
  ;; https://groups.google.com/g/racket-users/c/ZbYRQCy93dY/m/kF_Ek0VvAQAJ
  (define (indexof-table sym res-pict)
    (make-2table
      #:row-sep med-y-sep
      #:col-align lc-superimpose
      (list
        (cons (make-index-of sym)
              res-pict))))
  (define bottom-y 80/100)
  (define tbl-x 25/100)
  (pslide
    #:go heading-text-coord
    @t{Deep types can be weird}
    #:go (coord tbl-x 20/100 'lt)
    #:alt [(indexof-table 'untyped @t{0})]
    (vc-append
      med-y-sep
      (indexof-table 'deep @t{#f})
      (tag-pict (blank) 'anchor))
    #:go (at-find-pict 'anchor ct-find 'ct)
    @tcodesize{because (not (equal? 'a #<A4>)) ... of course})
  (pslide
    #:go heading-text-coord
    @t{Shallow types are more permissive}
    #:go (coord tbl-x 20/100 'lt)
    (vc-append
      med-y-sep
      (indexof-table 'shallow @t{0})
      (tag-pict (blank) 'anchor))
    #:go (at-find-pict 'anchor ct-find 'ct)
    @tcodesize{because (equal? 'a 'a)})
  (void))

(define (sec:shallow-bad)
  ;; not going to replace Deep TR
  ;; ... here are reasons why not
  ;; (end up again with reasons for)
  (pslide
    #:go slogan-coord
    (make-slogan (list catchphrase-sym chap-sym pro-sym)))
  (pslide
    #:go (coord 1/2 1/2 'cc)
    (vc-append
      med-y-sep
      @t{Hang on ...}
      (bgbg (bitmap "src/devils-contract.jpg"))))
  (pslide
    #:go heading-text-coord
    @t{Shallow types can be slow!}
    #:go (coord slide-text-left 20/100 'lt)
    (make-2table
      #:row-sep med-y-sep
      #:col-align lc-superimpose
      (list
        (cons (make-sieve 'deep 'deep)
              @t{<2 sec.})
        (cons (make-sieve 'shallow 'shallow)
              @t{~5 sec.})))
    )
  (pslide
    #:go heading-text-coord
    @t{Deep vs. Shallow, fully-typed}
    #:go (coord 1/2 20/100 'ct)
    (make-datatable
      (list
        (list "Benchmark" "Deep/Untyped" "Shallow/Untyped")
        (list "sieve" "1x" "2x")
        (list "fsmoo" "0.9x" "4x")
        (list "dungeon" "1x" "5x")
        (list "mbta" "2x" "2x")
        (list "tetris" "0.9x" "8x")
        (list "synth" "0.9x" "4x"))))
  (pslide
    #:go heading-text-coord
    @t{Shallow types have limited scope}
    #:go (coord slide-text-left slide-text-top 'lt)
    (make-repl-table
      (list
        (cons
          (make-shallow-codeblock*
            #:title Shallow-TR
            (list
              @ct{(define (make-random-fn)}
              @ct{  : (-> String)}
              @ct{  ....)}
              repl-bot-wedge))
          (make-untyped-codeblock*
            #:title repl
            (list
              @ct{> ((make-random-fn))}
              @ct{42}
              )))
        (cons
          (blank)
          (make-shallow-codeblock*
            #:title S-repl
            (list
              @ct{> ((make-random-fn))}
              @ct{contract error}
              ))))))
  (void))

(define (sec:cometh-soon)
  (pslide
    #:go slogan-coord
    #:alt [(make-slogan (list catchphrase-sym chap-sym pro-sym))]
    (make-slogan (list catchphrase-sym chap-sym pro-sym con-sym)))
  (pslide
    #:go heading-text-coord @t{Recommendations}
    #:go (coord 1/2 24/100 'ct #:sep small-y-sep)
    #:alt [(migration-rectangle (* 90/100 client-w) (* 20/100 client-h))]
    #:alt [(migration-rectangle (* 90/100 client-w) (* 20/100 client-h) #:flag '(shallow))]
    (migration-rectangle (* 90/100 client-w) (* 20/100 client-h) #:flag '(shallow deep))
    #:next
    #:go (coord 1/2 61/100 'ct #:sep pico-y-sep)
    (make-usecase-label "migrate: first Shallow, then Deep")
    (make-migration-path))
  (pslide
    #:go slogan-coord
    (make-slogan (list catchphrase-sym chap-sym pro-sym con-sym cometh-sym)))
  (void))

;; =============================================================================

(module+ main
  (do-show))

;; =============================================================================

(module+ raco-pict (provide raco-pict) (define client-w 984) (define client-h 728) (define raco-pict
  (ppict-do (filled-rectangle client-w client-h #:draw-border? #f #:color background-color)

    #:go (coord slide-text-left slide-text-top 'lt)
    (blank)


  )))

