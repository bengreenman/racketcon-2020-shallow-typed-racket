users
===

Issues from racket-users list that Shallow types fix, no problem
(see dissertation/QA/transient-expressive for details)

Basics, motivating:

- https://groups.google.com/g/racket-users/c/cCQ6dRNybDg/m/CKXgX1PyBgAJ
  any to proc, no "higher order value" problems anymore
- https://groups.google.com/g/racket-users/c/ZbYRQCy93dY/m/kF_Ek0VvAQAJ
  parametric contract changes result, transient avoids

- https://groups.google.com/g/racket-users/c/jtmVDFCGL28/m/jwl4hsjtBQAJ
  cannot protect opaque value, FIXED by transient
  "And I forgot: What is the cryptic error message 'any-wrap/c: Unable to
   protect opaque value passed as `Any`' telling me?"


Extra slides

- https://groups.google.com/g/racket-users/c/2X5olKMV3C4/m/mJhsp9ZWBgAJ
  inference gets precise type, cast does not forget it, FIXED by transient b/c old type forgotten
- https://groups.google.com/g/racket-users/c/UD20HadJ9Ec/m/Lmuw0U8mBwAJ
  set! has no effect b/c Deep contract forces a copy. Shallow = no copy


Unused, probably

- https://groups.google.com/g/racket-users/c/BDrrgW0axGQ/m/P31NxeGHAAAJ
  require/typed case-> with 2 arity-1 cases, ok for transient

