racket-con-2020
===

Talk for chaperone racket-con

Speaker instructions:
 <https://docs.google.com/document/d/1nApAL9nPfawk7OaVfimOBbdTwUOYGcd-bbIIstC8nS0/edit?usp=sharing>

15min talk, 10min QA, sitting in 30min slot

- [X] title + abstract + bio, due 2020-08-21
- [X] pick time, 2020-08-18 (sunday ... 2nd to last talk)
- [X] draft, 2020-09-08
  - may want https://obsproject.com/
- [X] rehearse, by 2020-09-18
- [X] record 1, 2020-10-02
- [ ] record 2, 2020-10-12
- [ ] showing 2020-10-18, 16:30 EST, hallway until 18:00


title =
Shallow Typed Racket


abstract =
Typed Racket adds a new dimension to Racket; any module in a program can be
strengthened with full-power static types. Good! But powerful types rely on
contracts, and these contracts can make a program run unusably slow. Not good!

Shallow Typed Racket adds a new dimension to Typed Racket. Instead of deep
and powerful types, Shallow offers weak types. And instead of potentially-devastating
run-time overhead, shallow types charge a small fee for every line of typed
code. Your code of tomorrow can mix deep and shallow types to find the best fit.


bio =
Ben Greenman is a final-semester Ph.D. student at Northeastern University.
Next year, he will be studying how programmers interact with contracts and types.
Reach out if you've been puzzling over blame errors.


